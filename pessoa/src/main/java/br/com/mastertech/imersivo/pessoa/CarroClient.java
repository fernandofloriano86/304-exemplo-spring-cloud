package br.com.mastertech.imersivo.pessoa;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "carro")
public interface CarroClient {
	
	@GetMapping("/carro/{modelo}")
	public Carro criaCarro(@PathVariable String modelo);
	
}	
